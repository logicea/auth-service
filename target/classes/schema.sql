-- Change Schema To Meet DB Specs
create table if not exists oauth_client_details
(
    client_id               varchar(64) not null
        primary key,
    access_token_validity   integer,
    additional_information  varchar(255),
    authorities             varchar(255),
    authorized_grant_types  varchar(255),
    autoapprove             varchar(255),
    client_secret           varchar(255),
    refresh_token_validity  integer,
    resource_ids            varchar(255),
    scope                   varchar(255),
    web_server_redirect_uri varchar(255)
);