-- auto-generated definition
create table users
(
    id                 bigint auto_increment
        primary key,
    created_by         varchar(255)                   null,
    created_on         datetime(6)                    null,
    last_modified_by   varchar(255)                   null,
    last_modified_date datetime(6)                    null,
    soft_delete        tinyint(1)   default 0         null,
    active             tinyint(1)   default 0         null,
    blocked            tinyint(1)   default 1         null,
    device_id          varchar(255)                   null,
    first_time_login   tinyint(1)   default 0         null,
    last_login         datetime(6)                    null,
    login_attempts     int                            not null,
    password           varchar(255)                   null,
    demo_pin           varchar(70)                    null,
    username           varchar(255)                   null,
    roles_id           bigint                         null,
    change_pin         tinyint(1)   default 1         not null,
    channel            varchar(255)                   null,
    email              varchar(255)                   null,
    language           varchar(255) default 'english' null,
    phone_number       varchar(20)                    null,
    set_sec_qns        tinyint(1)   default 1         null,
    country_code       varchar(55)                    null,
    constraint username
        unique (username),
    constraint FK8gpj7stba9dpshigjfvxyh6td
        foreign key (roles_id) references user_roles (id)
);