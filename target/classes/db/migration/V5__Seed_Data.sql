-- auto-generated definition
create table message_templates
(
    id                 bigint auto_increment
        primary key,
    created_by         varchar(255)         null,
    created_on         datetime(6)          null,
    last_modified_by   varchar(255)         null,
    last_modified_date datetime(6)          null,
    soft_delete        tinyint(1) default 0 null,
    language           varchar(255)         null,
    template           varchar(255)         null,
    type               varchar(255)         null
);

