package io.eclectics.sessionsauthserver.service.impl;

import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.other.UniversalResponse;
import io.eclectics.sessionsauthserver.repository.MessageTemplatesRepository;
import io.eclectics.sessionsauthserver.repository.PinTrackRepository;
import io.eclectics.sessionsauthserver.repository.RoleRepository;
import io.eclectics.sessionsauthserver.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class UserServiceImplTest {
    @Mock
    UserRepository userRepository;
    @Mock
    MessageTemplatesRepository messageTemplatesRepository;
    @Mock
    RoleRepository roleRepository;
    @Mock
    PinTrackRepository pinTrackRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    Logger log;
    @InjectMocks
    UserServiceImpl userServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void checkPassword_throwsBadCredentialsExceptionWhenPasswordIsIncorrect() {
        // given
        String password = "wrong password";
        Users user = new Users();
        user.setId(1);
        user.setLoginAttempts(0);
        user.setBlocked(false);
        user.setUsername("254712345678");
        user.setPassword("$2a$10$hyga3SbsUIhe42/zcmqcLO0ISc87o7fGKOYwmI85zRpYZVCAAHwhy"); // password

        // when
        when(userRepository.save(user)).thenReturn(Mono.just(user));
        Mono<Users> usersMono = userServiceImpl.checkPassword(user, password);

        //then
        StepVerifier.create(usersMono)
                .expectError(BadCredentialsException.class)
                .verify();
    }

    @Test
    void checkPassword_returnsUserDetailsIfPasswordIsCorrect() {
        // given
        String password = "password";
        Users user = new Users();
        user.setId(1);
        user.setLoginAttempts(0);
        user.setBlocked(false);
        user.setUsername("254712345678");
        user.setPassword("$2a$10$hyga3SbsUIhe42/zcmqcLO0ISc87o7fGKOYwmI85zRpYZVCAAHwhy"); // password

        // when
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        Mono<Users> usersMono = userServiceImpl.checkPassword(user, password);

        //then
        StepVerifier.create(usersMono)
                .assertNext(u -> {
                    assertEquals(u.getUsername(), user.getUsername());
                    assertEquals(u.getId(), user.getId());
                    assertThat(passwordEncoder.matches(password, u.getPassword())).isTrue();
                })
                .verifyComplete();

        verify(userRepository, never()).save(any());
    }


    @Test
    void checkLoginAttempts_throwsBadCredentialsWhenTrialsExceeded() {
        // given
        Users user = new Users();
        user.setId(1);
        user.setLoginAttempts(3);
        user.setBlocked(false);
        user.setUsername("254712345678");
        user.setPassword("$2a$10$hyga3SbsUIhe42/zcmqcLO0ISc87o7fGKOYwmI85zRpYZVCAAHwhy"); // password

        // when
        when(userRepository.save(user)).thenReturn(Mono.just(user));
        Mono<Users> usersMono = userServiceImpl.checkLoginAttempts(user);

        // then
        StepVerifier.create(usersMono)
                .expectError(BadCredentialsException.class)
                .verify();
    }

    @Test
    void checkLoginAttempts_doesNotThrowBadCredentialsWhenTrialsNotExceeded() {
        // given
        Users user = new Users();
        user.setId(1);
        user.setLoginAttempts(2);
        user.setBlocked(false);
        user.setUsername("254712345678");
        user.setPassword("$2a$10$hyga3SbsUIhe42/zcmqcLO0ISc87o7fGKOYwmI85zRpYZVCAAHwhy"); // password

        // when
        when(userRepository.save(user)).thenReturn(Mono.just(user));
        Mono<Users> usersMono = userServiceImpl.checkLoginAttempts(user);

        // then
        StepVerifier.create(usersMono)
                .assertNext(u -> {
                    assertEquals(u.getUsername(), user.getUsername());
                    assertEquals(u.getId(), user.getId());
                    assertEquals(u.getPassword(), user.getPassword());
                })
                .verifyComplete();
    }
}

