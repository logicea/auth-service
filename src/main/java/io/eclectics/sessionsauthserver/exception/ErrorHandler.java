package io.eclectics.sessionsauthserver.exception;

import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import reactor.core.publisher.Mono;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(RuntimeException.class)
    public Mono<ResponseEntity<AccessTokenWrapper>> handleApiError(RuntimeException e) {
        log.error(e.getMessage(), e);
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new AccessTokenWrapper(400, "Service not available at the moment")));
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public Mono<ResponseEntity<AccessTokenWrapper>> handleIllegalArgumentException(IllegalArgumentException e) {
        return Mono.just(new ResponseEntity<>(new AccessTokenWrapper(400, e.getMessage()), HttpStatus.OK));
    }

    @ExceptionHandler(value = {BadCredentialsException.class})
    public Mono<ResponseEntity<AccessTokenWrapper>> handleBadCredentialsException(BadCredentialsException e) {
        return Mono.just(new ResponseEntity<>(new AccessTokenWrapper(400, e.getMessage()), HttpStatus.OK));
    }

    @ExceptionHandler(value = {ClientIdException.class})
    public Mono<ResponseEntity<AccessTokenWrapper>> handleBadCredentialsException(ClientIdException e) {
        return Mono.just(new ResponseEntity<>(new AccessTokenWrapper(401, e.getMessage()), HttpStatus.OK));
    }

    @ExceptionHandler(value = {InvalidCredentialsException.class})
    public Mono<ResponseEntity<AccessTokenWrapper>> handleBadCredentialsException(InvalidCredentialsException e) {
        return Mono.just(new ResponseEntity<>(new AccessTokenWrapper(401, e.getMessage()), HttpStatus.OK));
    }
}
