package io.eclectics.sessionsauthserver.exception;

public class ClientIdException extends RuntimeException {
    public ClientIdException(String message) {
        super(message);
    }
}
