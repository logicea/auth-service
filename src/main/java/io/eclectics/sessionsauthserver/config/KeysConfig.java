package io.eclectics.sessionsauthserver.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class KeysConfig {
    private final AppProperties appProperties;
    @Bean
    public RSAPublicKey publicKey(KeyStore keyStore) {
        try {
            Certificate certificate = keyStore.getCertificate(appProperties.getAuth().getKeyAlias());
            PublicKey publicKey = certificate.getPublicKey();

            if (publicKey instanceof RSAPublicKey) {
                return (RSAPublicKey) publicKey;
            }
        } catch (KeyStoreException e) {
            log.error("Unable to load private key from keystore: {}", appProperties.getAuth().getKeystoreName(), e);
        }
        throw new IllegalArgumentException("Unable to load RSA public key");
    }

    @Bean
    public RSAPrivateKey privateKey(KeyStore keyStore) {
        try {
            ClassPathResource resource = new ClassPathResource(appProperties.getAuth().getKeystoreName());
            keyStore.load(resource.getInputStream(), appProperties.getAuth().getKeystorePassword().toCharArray());

            return (RSAPrivateKey) keyStore.getKey(appProperties.getAuth().getKeyAlias(), appProperties.getAuth().getKeystorePassword().toCharArray());
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException |
                 UnrecoverableKeyException e) {
            log.error("Unable to load keystore: {}", appProperties.getAuth().getKeystoreName(), e);
        }
        throw new IllegalArgumentException("Unable to load keystore");
    }

    @Bean
    public KeyStore keyStore() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            ClassPathResource resource = new ClassPathResource(appProperties.getAuth().getKeystoreName());
            keyStore.load(resource.getInputStream(), appProperties.getAuth().getKeystorePassword().toCharArray());
            return keyStore;
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            log.error("Unable to load keystore: {}", appProperties.getAuth().getKeystoreName(), e);
        }

        throw new IllegalArgumentException("Unable to load keystore");
    }
}
