package io.eclectics.sessionsauthserver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * A spring configuration properties class.
 * Useful for avoiding @Value annotation.
 */
@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private final Auth auth = new Auth();

    /**
     * Is inflated by Spring at runtime.
     * Configuration properties in Yaml file are inflated
     * here.
     */

    @Getter
    @Setter
    public static class Auth {
        private String keystoreName;
        private String keystorePassword;
        private String keyAlias;
    }

    public Auth getAuth() {
        return auth;
    }
}
