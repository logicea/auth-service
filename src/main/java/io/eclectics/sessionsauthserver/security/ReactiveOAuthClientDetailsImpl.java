package io.eclectics.sessionsauthserver.security;

import io.eclectics.sessionsauthserver.model.OauthClientDetails;
import io.eclectics.sessionsauthserver.repository.OAuthClientRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * An implementation of ReactiveUserDetails.
 */
@Slf4j
@Component
@AllArgsConstructor
public class ReactiveOAuthClientDetailsImpl implements ReactiveUserDetailsService {

    private final OAuthClientRepository oAuthClientRepository;

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return oAuthClientRepository.findByClientId(username)
                .filter(Objects::nonNull)
                .switchIfEmpty(Mono.error(new BadCredentialsException("Invalid client id!")))
                .map(this::createSpringSecurityUser);
    }

    private User createSpringSecurityUser(OauthClientDetails clientDetails) {
        List<GrantedAuthority> grantedAuthorities =
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT"));
        return new User(clientDetails.getClientId(), clientDetails.getClientSecret(), grantedAuthorities);
    }

}
