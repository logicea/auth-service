package io.eclectics.sessionsauthserver.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import io.eclectics.sessionsauthserver.config.AppProperties;
import io.eclectics.sessionsauthserver.constants.Channels;
import io.eclectics.sessionsauthserver.exception.ClientIdException;
import io.eclectics.sessionsauthserver.model.OauthClientDetails;
import io.eclectics.sessionsauthserver.model.Roles;
import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.repository.OAuthClientRepository;
import io.eclectics.sessionsauthserver.security.interfaces.TokenProvider;
import io.eclectics.sessionsauthserver.service.TokenStore;
import io.eclectics.sessionsauthserver.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import scala.Function0;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;

/**
 * A class that is responsible for generation and validation of JWT.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class JWTProviderImpl implements TokenProvider {

    public static final String USER_NAME = "user_name";
    private final UserService userService;
    private final OAuthClientRepository oAuthClientRepository;
    private static final String AUTHORITIES = "authorities";
    private static final String CLIENT_ID = "client_id";
    private static final String JTI = "jti";
    private static final String ATI = "ati";
    private static final String SCOPE = "scope";
    private static final String USERNAME = USER_NAME;

    private final RSAPublicKey rsaPublicKey;
    private final RSAPrivateKey privateKey;
    private final AppProperties appProperties;
    private final TokenStore tokenStore;

    /**
     * Driver method
     *
     * @param authentication the client that is currently logged in
     * @param user           The user to validate
     * @return A token and refresh token and other info. See AccessTokenWrapper for more.
     */
    @Override
    public Mono<AccessTokenWrapper> generateToken(Authentication authentication, Users user) {
        return oAuthClientRepository.findByClientId(authentication.getName())
                .flatMap(clientDetails -> generateToken(clientDetails, user, authentication.getName()));
    }

    /**
     * Helper method that does the generation of JWT
     *
     * @param clientDetails The currently signed in client. Has info about the longevity of the tokens.
     * @param user          The user to validate.
     * @param clientId
     * @return An access token and response token.
     */

    private Mono<AccessTokenWrapper> generateToken(OauthClientDetails clientDetails, Users user, String clientId) {
        return Mono.fromCallable(() -> {
            Calendar calendar = Calendar.getInstance();
            Roles roles = user.getRoles();

            List<String> simpleGrantedAuthorities = Collections.singletonList(roles.getName());

            Date now = calendar.getTime();
            String username = user.getChannel().equals(Channels.APP.name()) ? user.getPhoneNumber() : user.getEmail();

            if (username.matches("\\d+") && !Objects.equals(roles.getResourceId(), clientId))
                throw new ClientIdException("Client id does not match user resource");

            if (username.contains("@") && !Objects.equals(roles.getResourceId(), clientId))
                throw new ClientIdException("Client id does not match user resource");

            String jti = UUID.randomUUID().toString();
            Date expiryDate = new Date(now.getTime() + (int) (clientDetails.getAccessTokenValidity() * 1000));
            List<String> scopes = Arrays.asList(clientDetails.getScope().split(","));

            JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                    .claim(USERNAME, username)
                    .claim(SCOPE, scopes)
                    .claim(JTI, jti)
                    .claim(CLIENT_ID, clientDetails.getClientId())
                    .claim(AUTHORITIES, simpleGrantedAuthorities)
                    .audience(Collections.singletonList(clientDetails.getResourceIds()))
                    .expirationTime(expiryDate)
                    .notBeforeTime(now)
                    .issueTime(now)
                    .build();

            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
                    .type(JOSEObjectType.JWT)
                    .build();

            SignedJWT signedJWT = new SignedJWT(jwsHeader, claimsSet);

            RSASSASigner signer = new RSASSASigner(privateKey);

            signedJWT.sign(signer);

            return AccessTokenWrapper.builder()
                    .access_token(signedJWT.serialize())
                    .username(username)
                    .refresh_token(generateRefreshToken(clientDetails, user))
                    .token_type(JOSEObjectType.JWT.getType())
                    .expires_in(expiryDate.getTime() - new Date().getTime())
                    .setSecQns(user.isSetSecQns())
                    .changePin(user.isChangePin())
                    .language(user.getLanguage())
                    .scope(String.join(" ", scopes))
                    .jti(jti)
                    .message("Logged in successfully")
                    .status(200)
                    .build();
        });
    }

    public String generateRefreshToken(OauthClientDetails clientDetails, Users user) {
        List<String> simpleGrantedAuthorities = Collections.singletonList("ROLE_REFRESH");

        Date now = new Date();
        String username = user.getChannel().equals("APP") ? user.getPhoneNumber() : user.getEmail();
        String jti = UUID.randomUUID().toString();
        String ati = UUID.randomUUID().toString();
        Date expiryDate = new Date(now.getTime() + (clientDetails.getAccessTokenValidity() + Duration.ofMinutes(5).toSeconds()) * 1000);
        List<String> scopes = Arrays.asList(clientDetails.getScope().split(","));

        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .claim(USERNAME, username)
                .claim(SCOPE, scopes)
                .claim(JTI, jti)
                .claim(ATI, ati)
                .claim(CLIENT_ID, clientDetails.getClientId())
                .claim(AUTHORITIES, simpleGrantedAuthorities)
                .audience(Collections.singletonList(clientDetails.getResourceIds()))
                .expirationTime(expiryDate)
                .notBeforeTime(now)
                .issueTime(now)
                .build();

        JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .type(JOSEObjectType.JWT)
                .build();

        SignedJWT signedJWT = new SignedJWT(jwsHeader, claimsSet);

        RSASSASigner signer = new RSASSASigner(privateKey);

        try {
            signedJWT.sign(signer);
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }

        return signedJWT.serialize();
    }

    @Override
    public Mono<AccessTokenWrapper> validateToken(String token) {
        return Mono.fromCallable(() -> {
            SignedJWT signedJWT = SignedJWT.parse(token);

            RSASSAVerifier rsassaVerifier = new RSASSAVerifier(rsaPublicKey);

            if (!signedJWT.verify(rsassaVerifier)) {
                return new AccessTokenWrapper(401, "Failed to verify token!");
            }

            String userName = (String) signedJWT.getJWTClaimsSet().getClaim(USER_NAME);
            Date tokenExpiry = (Date) signedJWT.getJWTClaimsSet().getClaim("exp");

            Optional<String> optionalToken = tokenStore.findToken(userName).blockOptional();

            if (optionalToken.isEmpty()) {
                return new AccessTokenWrapper(400, "User token not found in store!");
            }

            String savedToken = optionalToken.get();
            if (!savedToken.equals(String.valueOf(token.hashCode()))) {
                return new AccessTokenWrapper(400, "Token does not match the one in store!");
            }

            Date now = new Date();
            if (tokenExpiry.before(now)) {
                return new AccessTokenWrapper(401, "Token is expired!");
            }

            return new AccessTokenWrapper(200, "Token is valid!", userName);
        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<AccessTokenWrapper> validateRefreshToken(String token) {
        return Mono.fromCallable(() -> {
            SignedJWT signedJWT = SignedJWT.parse(token);

            RSASSAVerifier rsassaVerifier = new RSASSAVerifier(rsaPublicKey);

            if (!signedJWT.verify(rsassaVerifier)) {
                return new AccessTokenWrapper(401, "Failed to verify token!");
            }

            String userName = (String) signedJWT.getJWTClaimsSet().getClaim(USER_NAME);
            List<String> authorities = (List<String>) signedJWT.getJWTClaimsSet().getClaim(AUTHORITIES);
            Date tokenExpiry = (Date) signedJWT.getJWTClaimsSet().getClaim("exp");

            Optional<String> optionalRefreshToken = tokenStore.findRefreshToken(userName).blockOptional();

            if (authorities.stream().noneMatch(s -> s.equals("ROLE_REFRESH"))) {
                return new AccessTokenWrapper(400, "Token authority not valid");
            }

            if (optionalRefreshToken.isEmpty()) {
                return new AccessTokenWrapper(400, "User token not found in store!");
            }

            String refreshToken = optionalRefreshToken.get();

            if (!refreshToken.equals(String.valueOf(token.hashCode()))) {
                return new AccessTokenWrapper(400, "Token does not match the one in store!");
            }

            Date now = new Date();
            if (tokenExpiry.before(now)) {
                return new AccessTokenWrapper(401, "Token is expired!");
            }

            return new AccessTokenWrapper(200, "Token is valid!", userName);
        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<AccessTokenWrapper> refreshToken(String token, AccessTokenWrapper validationRes, Authentication authentication) {
        Mono<Users> userMono = userService.getUserByUsername(validationRes.getUsername());

        return Mono.just(validationRes)
                .filter(res -> res.getStatus() == 200)
                .switchIfEmpty(Mono.defer(() -> raiseTokenNotValid(validationRes.getMessage())))
                .zipWith(userMono)
                .flatMap(tuple -> generateToken(authentication, tuple.getT2()));
    }

    private <T> Mono<T> raiseTokenNotValid(String msg) {
        return Mono.error(new BadCredentialsException(msg));
    }
}
