package io.eclectics.sessionsauthserver.security;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

/**
 * Responsible for handling user login.
 * Uses the Reactive Authentication Manager.
 */
@Slf4j
@AllArgsConstructor
public class BasicAuthenticationManager implements ReactiveAuthenticationManager {

    private final ReactiveUserDetailsService userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public BasicAuthenticationManager(ReactiveUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        log.info("The user -> " + authentication.getPrincipal());
        if (authentication.isAuthenticated()) return Mono.just(authentication);

        return checkUser((String) authentication.getPrincipal(), (String) authentication.getCredentials())
                .map(userEntity -> toUsernamePasswordAuthenticationToken(authentication, userEntity));
    }

    public Mono<UserDetails> checkUser(String username, String credentials) {
        return userDetailsService.findByUsername(username)
                .flatMap(userEntity -> validateUser(userEntity, credentials))
                .switchIfEmpty(Mono.defer(this::raiseBadCredentials));
    }

    public Mono<UserDetails> validateUser(UserDetails userDetails, String credentials) {
        if (!passwordEncoder.matches(credentials, userDetails.getPassword())) {
            return Mono.defer(this::raiseBadCredentials);
        }
        return Mono.just(userDetails);
    }

    private UsernamePasswordAuthenticationToken toUsernamePasswordAuthenticationToken(Authentication authentication, UserDetails userDetails) {
        List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_CLIENT"));
        return new UsernamePasswordAuthenticationToken(
                authentication.getPrincipal(),
                authentication.getCredentials(),
                authorities
        );
    }

    private <T> Mono<T> raiseBadCredentials() {
        return Mono.error(new BadCredentialsException("Bad credentials!!"));
    }
}
