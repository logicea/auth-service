package io.eclectics.sessionsauthserver.security.interfaces;

import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

/**
 * An interface for providing spring security user details.
 */
public interface ReactiveOAuthClientDetails {

    Mono<UserDetails> findByUsername(String username);

}
