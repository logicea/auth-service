package io.eclectics.sessionsauthserver.security.interfaces;

import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

public interface TokenProvider {
    Mono<AccessTokenWrapper> generateToken(Authentication authentication, Users user);

    Mono<AccessTokenWrapper> validateToken(String token);

    Mono<AccessTokenWrapper> validateRefreshToken(String token);

    Mono<AccessTokenWrapper> refreshToken(String token, AccessTokenWrapper validationRes, Authentication authentication);
}
