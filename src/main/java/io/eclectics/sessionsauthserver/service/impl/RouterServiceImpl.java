package io.eclectics.sessionsauthserver.service.impl;

import com.google.gson.Gson;
import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.*;
import io.eclectics.sessionsauthserver.repository.UserRepository;
import io.eclectics.sessionsauthserver.security.interfaces.TokenProvider;
import io.eclectics.sessionsauthserver.service.RouterService;
import io.eclectics.sessionsauthserver.service.TokenStore;
import io.eclectics.sessionsauthserver.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RouterServiceImpl implements RouterService {

    private final UserService userService;

    private final TokenProvider tokenProvider;

    private final TokenStore tokenStore;

    @Override
    public Mono<ResponseEntity<AccessTokenWrapper>> loginUser(String username, String password, String grantType, Authentication authentication) {
        return Mono.just(username)
                .flatMap(uname -> userService.validateLogin(uname, password)
                        .flatMap(user -> tokenProvider.generateToken(authentication, user))
                        .doOnNext(accessTokenWrapper -> tokenStore.saveToken(accessTokenWrapper, username)
                                .subscribeOn(Schedulers.boundedElastic())
                                .subscribe())
                        .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res)));
    }
    @Override
    public Mono<AccessTokenWrapper> refreshToken(String token, Authentication authentication) {
        return tokenProvider.validateRefreshToken(token).flatMap(validationRes -> tokenProvider.refreshToken(token, validationRes, authentication)).doOnNext(accessTokenWrapper -> tokenStore.saveToken(accessTokenWrapper, accessTokenWrapper.getUsername()).subscribeOn(Schedulers.boundedElastic()).subscribe());
    }

}
