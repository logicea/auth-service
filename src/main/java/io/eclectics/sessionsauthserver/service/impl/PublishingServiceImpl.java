package io.eclectics.sessionsauthserver.service.impl;


import com.google.gson.Gson;
import io.eclectics.sessionsauthserver.constants.KafkaTopics;
import io.eclectics.sessionsauthserver.service.PublishingService;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class PublishingServiceImpl implements PublishingService {
    private final static Logger logger = Logger.getLogger(PublishingServiceImpl.class.getName());
    static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy HH:mm:ss");
    private final StreamBridge streamBridge;
    private final Gson gson;

    @Override
    public void sendText(String userName, String ipAddress, String hostName) {
        Map<String, String> logAuthPayload = new HashMap<>();

        logAuthPayload.put("AccessId", "25325");
        logAuthPayload.put("ServiceName", "file_service");
        logAuthPayload.put("AccessName", userName);
        logAuthPayload.put("AccessType", "MOBILE");
        logAuthPayload.put("AccessProfile", "ADMIN");
        logAuthPayload.put("AccessCounty", "KE");
        logAuthPayload.put("AccessSourceIp", ipAddress);
        logAuthPayload.put("AccessSourceHost", hostName);
        logAuthPayload.put("AccessLoginTimeStamp", formatter.format(LocalDateTime.now()));
        logAuthPayload.put("AccessLogoutTimeStamp", "");
        logAuthPayload.put("AccessAppStatus", "OK");
        logAuthPayload.put("SessionId", UUID.randomUUID().toString());

        logger.info("payload " + logAuthPayload);
        streamBridge.send(KafkaTopics.AUTH_AUDIT, gson.toJson(logAuthPayload));
    }
}
