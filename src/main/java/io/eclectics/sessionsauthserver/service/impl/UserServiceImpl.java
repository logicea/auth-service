package io.eclectics.sessionsauthserver.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.eclectics.sessionsauthserver.constants.Channels;
import io.eclectics.sessionsauthserver.exception.InvalidCredentialsException;
import io.eclectics.sessionsauthserver.model.PinTrack;
import io.eclectics.sessionsauthserver.model.Roles;
import io.eclectics.sessionsauthserver.constants.UserType;
import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.other.DemoPinWrapper;
import io.eclectics.sessionsauthserver.other.UniversalResponse;
import io.eclectics.sessionsauthserver.repository.MessageTemplatesRepository;
import io.eclectics.sessionsauthserver.repository.PinTrackRepository;
import io.eclectics.sessionsauthserver.repository.RoleRepository;
import io.eclectics.sessionsauthserver.repository.UserRepository;
import io.eclectics.sessionsauthserver.service.UserService;
import io.eclectics.sessionsauthserver.util.GeneralUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;

import static io.eclectics.sessionsauthserver.constants.KafkaTopics.EMAIL_TOPIC;
import static io.eclectics.sessionsauthserver.constants.KafkaTopics.SMS_TOPIC;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    public static final String PHONE_NUMBER = "phoneNumber";
    private final UserRepository userRepository;
    private final MessageTemplatesRepository messageTemplatesRepository;
    private final RoleRepository roleRepository;

    private final PinTrackRepository pinTrackRepository;
    private final PasswordEncoder passwordEncoder;

    private final StreamBridge streamBridge;
    private final Gson gson = new Gson();

    private static final String DISABLE_MEMBER_CHANNEL = "deactivate-member-account-topic";
    private static final String OLD_PIN = "Dear %s, your previous password not found.";
    private static final String FTP_PIN = "Dear %s, Don't use First time pin as your new Pin";

    private static final String PREV_PIN = "Dear %s, your previous password should not match the new password. Kindly use a different one.";
    private static final String FIRST_TIME_MESSAGE = "Dear customer, please use %s as your first time pin in order to login to the app.";
    private static final String PORTAL_TIME_MESSAGE = "Dear customer, please use %s as your first time password to login to your portal link %s.";

    @Override
    public Mono<List<GrantedAuthority>> loadUserRoles(Users user) {
        return Mono.just(user).flatMap(u -> roleRepository.findById(u.getRoleId())).map(role -> List.of(new SimpleGrantedAuthority(role.getName())));
    }

    @Override
    public Mono<Users> getUserByUsername(String username) {
        return userRepository.findByPhoneNumberAndActive(username, true)
                .switchIfEmpty(Mono.error(new InvalidCredentialsException("User not found ")))
                .onErrorResume(throwable -> userRepository.findByEmailAndActive(username, true))
                .switchIfEmpty(Mono.error(new InvalidCredentialsException("User not found")))
                .flatMap(users -> roleRepository.findById(users.getRoleId())
                        .zipWith(Mono.just(users))
                        .map(res -> {
                            Users u = res.getT2();
                            Roles r = res.getT1();
                            u.setRoles(r);
                            return u;
                        }));
    }

    @Override
    public Mono<Users> validateLogin(String username, String password) {
        return getUserByUsername(username)
                .filter(user -> !user.isBlocked())
                .switchIfEmpty(Mono.defer(this::raiseAccountLocked)) // Password is not validated, user is already blocked.
                .flatMap(this::checkLoginAttempts) // Will throw an exception if account is locked.
                .flatMap(user -> checkPassword(user, password)) // Will throw an exception if password is not valid.
                .flatMap(this::resetLoginAttemptsAndSetLastLogin); // Reset login attempts and set last login.
    }

    public Mono<Users> checkLoginAttempts(Users user) {
        if (user.getLoginAttempts() == 3) {
            user.setBlocked(true);
            user.setActive(false);

            saveUser(user);
            return Mono.defer(this::raiseAccountLocked);
        }
        return Mono.just(user);
    }

    public Mono<Users> checkPassword(Users user, String password) {
        if (!passwordEncoder.matches(password, user.getPassword())) {
            // increment trials
            long trials = user.getLoginAttempts() + 1;
            user.setLoginAttempts(trials);
            user.setBlocked(trials == 3);

            saveUser(user);
            return Mono.defer(() -> raiseTrialsIncreased(3L - user.getLoginAttempts()));
        }

        return Mono.just(user);
    }

    @Override
    public void updateLastLogin(String loggedPrincipal) {
        userRepository.findUsersByEmailOrPhoneNumber(loggedPrincipal, loggedPrincipal).doOnNext(user -> {
            user.setLastLogin(LocalDateTime.now());
            userRepository.save(user).subscribeOn(Schedulers.boundedElastic()).subscribe(u -> log.info("Updated user details"));
        }).subscribeOn(Schedulers.boundedElastic()).subscribe();
    }

    @Override
    public Mono<Users> searchUserByUsernameAndChannel(String username, String channel) {
        if (channel.equals("portal")) {
            return userRepository.findByEmailAndChannelEquals(username, "PORTAL");
        } else if (channel.equals("app")) {
            log.info("App user... {}", username);
            return userRepository.findByPhoneNumberAndChannelEquals(username, "APP");
        } else return Mono.empty();
    }

    @Async
    @Override
    public void deactivateUser(Users user) {
        user.setActive(false);
        userRepository.save(user).subscribeOn(Schedulers.boundedElastic()).subscribe();
        // publish event to disable member if any
        if (user.getChannel().equals(Channels.APP.toString())) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(PHONE_NUMBER, user.getPhoneNumber());
            streamBridge.send(DISABLE_MEMBER_CHANNEL, gson.toJson(jsonObject));
        }
    }

    @Override
    public void resetLoginAttempts(Users user) {
        user.setLoginAttempts(0);
        saveUser(user);
    }

    private Mono<Users> resetLoginAttemptsAndSetLastLogin(Users user) {
        user.setLoginAttempts(0);
        user.setLastLogin(LocalDateTime.now());
        return persistUser(user);
    }

    @Override
    public void saveUser(Users user) {
        userRepository.save(user)
                .doOnError(throwable -> log.info(throwable.getLocalizedMessage()))
                .subscribeOn(Schedulers.boundedElastic())
                .subscribe(u -> log.info("Saved user with id::: {}", u.getId()));
    }

    private Mono<Users> persistUser(Users user) {
        return userRepository.save(user)
                .doOnError(throwable -> log.info(throwable.getLocalizedMessage()))
                .doOnNext(u -> log.info("Persisted user::: {}", u.getId()));
    }

    @Bean
    @Override
    public Consumer<String> createUserCredentials() {
        return payload -> Mono.fromRunnable(() -> {
            log.info("The payload Create User Credentials => {}", payload);

            JsonObject jsonObject = gson.fromJson(payload, JsonObject.class);
            String username = jsonObject.get("username").getAsString();
            String userType = jsonObject.get("userType").getAsString();
            String portalUrl = jsonObject.get("portalUrl").getAsString();

            if (!username.matches("\\d+") && !username.contains("@") && username.length() < 2) {
                log.info("Phone Number is invalid");
                return;
            }
            Optional<Users> optionalUser = userRepository.findByUsername(username).blockOptional();

            optionalUser.ifPresent(user -> updateCredentials(username, userType, user, portalUrl));

            if (optionalUser.isEmpty()) {
                Users user = new Users();
                String firstTimePassword = null;
                user.setUsername(username);
                user.setEmail(username);
                user.setActive(true);
                user.setFirstTimeLogin(true);
                user.setChangePin(true);
                user.setCreatedOn(LocalDateTime.now());
                Optional<Roles> optionalRole;

                if (Objects.equals(userType, UserType.CUSTOMER.name())) {
                    optionalRole = roleRepository.findById(3L).blockOptional();
                    firstTimePassword = GeneralUtils.generate6DigitsOtp();
                    user.setChannel(Channels.PORTAL.toString());
                    user.setPhoneNumber(username);
                    user.setPassword(passwordEncoder.encode(firstTimePassword));
                    user.setDemoPin(firstTimePassword);

                } else if (Objects.equals(userType, UserType.ADMIN.name())) {
                    user.setEmail(username);
                    user.setChannel(Channels.PORTAL.toString());
                    optionalRole = roleRepository.findById(1L).blockOptional();
                    firstTimePassword = GeneralUtils.generate6DigitsOtp();
                    user.setPassword(passwordEncoder.encode(firstTimePassword));
                    user.setDemoPin(firstTimePassword);
                } else {
                    optionalRole = Optional.empty();
                }

                if (optionalRole.isEmpty()) {
                    log.info("User role not found");
                    return;
                }

                user.setRoleId(optionalRole.get().getId());
                log.info("The  user role Id => {}", optionalRole.get().getId());

                // Save the generated
                userRepository.save(user).block();
                if (username.matches("\\d+") && username.length() > 2) {
                    // is phone number
                    String msg = String.format(FIRST_TIME_MESSAGE, firstTimePassword);
                    streamBridge.send(SMS_TOPIC, Map.of(PHONE_NUMBER, username, "message", msg));
                } else if (username.contains("@")) {
                    // is an email
                    String msg = String.format(PORTAL_TIME_MESSAGE, firstTimePassword, portalUrl);
                    Map<String, String> emailPayload = new HashMap<>();
                    emailPayload.put("title", "First Time Pin");
                    emailPayload.put("message", msg);
                    emailPayload.put("email", username);
                    streamBridge.send(EMAIL_TOPIC, emailPayload);
                }
                log.info("User credentials created successfully!");
            }
        }).publishOn(Schedulers.boundedElastic()).subscribeOn(Schedulers.boundedElastic()).subscribe();
    }

    private void updateCredentials(String username, String userType, Users user, String portalUrl) {
        log.info("User {} exists", username);

        String pin = null;
        if (userType.equals(UserType.CUSTOMER.name())) {
            pin = GeneralUtils.generate4DigitsOTP();
            user.setPassword(passwordEncoder.encode(pin));
            user.setDemoPin(pin);
            user.setChangePin(true);
        } else if (userType.equals(UserType.ADMIN.name())) {
            pin = GeneralUtils.generate6DigitsOtp();
            user.setPassword(passwordEncoder.encode(pin));
            user.setDemoPin(pin);
            user.setChangePin(true);
        }
        //save user
        userRepository.save(user).block();

        if (username.matches("\\d+") && username.length() > 2) {
            // is phone number
            String msg = String.format(FIRST_TIME_MESSAGE, pin);
            streamBridge.send(SMS_TOPIC, Map.of(PHONE_NUMBER, username, "message", msg));
        } else if (username.contains("@")) {
            // is an email
            String msg = String.format(PORTAL_TIME_MESSAGE, pin, portalUrl);
            Map<String, String> emailPayload = new HashMap<>();
            emailPayload.put("title", "First Time Pin");
            emailPayload.put("message", msg);
            emailPayload.put("email", username);
            streamBridge.send(EMAIL_TOPIC, emailPayload);
        }
    }

    @Override
    public boolean checkIfNewPinExists(String newPin, String username) {
        PinTrack pinTrack = pinTrackRepository.findByUsername(username)
                .filter(pin -> {
                    log.info("New Pin matches ? " + passwordEncoder.matches(newPin, pin.getNewPin()));
                    return passwordEncoder.matches(newPin, pin.getNewPin());
                }).blockFirst();
        return pinTrack != null;
    }



    @Override
    public Mono<AccessTokenWrapper> demo(String username) {
        return Mono.fromCallable(() -> {
            Optional<Users> optionalUser = userRepository.findByPhoneNumber(username).blockOptional();
            if (optionalUser.isEmpty()) {
                return AccessTokenWrapper.builder()
                        .status(400)
                        .message("User not found")
                        .build();
            }
            Users users = optionalUser.get();
            userRepository.deleteById(users.getId()).block();

            return AccessTokenWrapper.builder()
                    .status(200)
                    .message("Success")
                    .build();
        }).publishOn(Schedulers.boundedElastic());
    }
    @Override
    public boolean validateUserPin(String username, String pin) {
        return userRepository.findByPhoneNumber(username).blockOptional()
                .map(user -> {
                    log.info("user name {} ", user.getPhoneNumber());
                    return passwordEncoder.matches(pin, user.getPassword());
                }).orElse(false);
    }

    @Override
    public Mono<UniversalResponse> getDemoPins(String username) {
        return Mono.fromCallable(() -> {
            Optional<DemoPinWrapper> optionalUser = userRepository.findByUsernameAndActiveOrderByIdDesc(username, true).blockOptional();
            if (optionalUser.isEmpty()) {
                return UniversalResponse.builder()
                        .status("01")
                        .message("User not found or inactive")
                        .build();
            }

            DemoPinWrapper demoPinWrapper = optionalUser.get();
            return UniversalResponse.builder()
                    .status("00")
                    .message("Success")
                    .data(demoPinWrapper)
                    .build();
        }).publishOn(Schedulers.boundedElastic());
    }

    private <T> Mono<T> raiseAccountLocked() {
        return Mono.error(new BadCredentialsException("User account is locked!"));
    }

    private <T> Mono<T> raiseTrialsIncreased(long trials) {
        return Mono.error(new BadCredentialsException("Wrong password. You have " + trials + " attempts left."));
    }

}
