package io.eclectics.sessionsauthserver.service.impl;

import com.google.gson.Gson;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.service.TokenStore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.time.Duration;

@Slf4j
@Service
@RequiredArgsConstructor
public class RedisTokenStore implements TokenStore {

    public static final String TOKEN_HASH_KEY = "token";
    public static final String REFRESH_HASH_KEY = "refresh";
    private final Gson gson;

    @Value("${token.expiry}")
    private int tokenExpiry;

    private ReactiveHashOperations<String, String, String> hashOperations;

    private final ReactiveStringRedisTemplate redisOperations;
    private static final String KEY_PREFIX = "ts-";


    @PostConstruct
    private void init() {
        hashOperations = redisOperations.opsForHash();
    }

    @Override
    public Mono<Boolean> saveToken(AccessTokenWrapper accessTokenWrapper, String username) {
        Mono<Boolean> token = hashOperations.put(KEY_PREFIX + username, TOKEN_HASH_KEY, String.valueOf(accessTokenWrapper.getAccess_token().hashCode()));
        Mono<Boolean> refresh = hashOperations.put(KEY_PREFIX + username, REFRESH_HASH_KEY, String.valueOf(accessTokenWrapper.getRefresh_token().hashCode()));

        return token.zipWith(refresh)
                .doOnError(throwable -> log.error(throwable.getLocalizedMessage(), throwable))
                .doOnNext(tuple2 -> log.info("Saved token and refresh token: {} {}", tuple2.getT1(), tuple2.getT2()))
                .flatMap(tuple2 -> redisOperations.expire(KEY_PREFIX + username, Duration.ofMinutes(tokenExpiry)))
                .doOnNext(bool -> log.info("Set expiry successfully? {}", bool));
    }

    @Override
    public Mono<Long> removeToken(String username) {
        return redisOperations.delete(KEY_PREFIX + username);
    }

    @Override
    public Mono<String> findToken(String username) {
        return hashOperations.get(KEY_PREFIX + username, TOKEN_HASH_KEY);
    }

    @Override
    public Mono<String> findRefreshToken(String username) {
        return hashOperations.get(KEY_PREFIX + username, REFRESH_HASH_KEY);
    }
}
