package io.eclectics.sessionsauthserver.service.impl;

import io.eclectics.sessionsauthserver.service.AuditService;
import io.eclectics.sessionsauthserver.service.PublishingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import java.util.Objects;

@Service
public class AuditServiceImpl implements AuditService {

    @Override
    public String extractIpAddress(ServerWebExchange request) {
       return Objects.requireNonNull(request.getRequest().getRemoteAddress()).getAddress().getHostAddress();
    }

    @Override
    public String extractRemoteHostName(ServerWebExchange request) {
        return Objects.requireNonNull(request.getRequest().getRemoteAddress()).getAddress().getHostName();

    }
}
