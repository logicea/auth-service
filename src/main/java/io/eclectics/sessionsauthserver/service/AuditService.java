package io.eclectics.sessionsauthserver.service;

import org.springframework.web.server.ServerWebExchange;

import java.util.function.Consumer;

public interface AuditService {

    String extractIpAddress(ServerWebExchange exchange);

    String extractRemoteHostName(ServerWebExchange request);
}
