package io.eclectics.sessionsauthserver.service;

import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import reactor.core.publisher.Mono;

public interface TokenStore {

    Mono<Boolean> saveToken(AccessTokenWrapper accessTokenWrapper, String username);

    Mono<Long> removeToken(String username);

    Mono<String> findToken(String username);

    Mono<String> findRefreshToken(String username);
}

