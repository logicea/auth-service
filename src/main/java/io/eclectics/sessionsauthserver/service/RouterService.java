package io.eclectics.sessionsauthserver.service;

import com.netflix.appinfo.ApplicationInfoManager;
import io.eclectics.sessionsauthserver.other.*;
import io.lettuce.core.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

public interface RouterService {
    Mono<ResponseEntity<AccessTokenWrapper>> loginUser(String username, String password, String grantType, Authentication authentication);
    Mono<AccessTokenWrapper> refreshToken(String token, Authentication authentication);

}
