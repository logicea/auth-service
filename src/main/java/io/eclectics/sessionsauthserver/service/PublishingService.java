package io.eclectics.sessionsauthserver.service;

public interface PublishingService {

    void sendText(String userName, String ipAddress, String hostName);
}
