package io.eclectics.sessionsauthserver.service;

import com.google.gson.JsonObject;
import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.other.UniversalResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

public interface UserService {
    Mono<List<GrantedAuthority>> loadUserRoles(Users user);

    Mono<Users> getUserByUsername(String username);

    Mono<Users> validateLogin(String username, String password);

    void updateLastLogin(String loggedPrincipal);

    Mono<Users> searchUserByUsernameAndChannel(String username, String channel);

    void deactivateUser(Users user);

    void resetLoginAttempts(Users user);

    void saveUser(Users user);

    Consumer<String> createUserCredentials();

    boolean checkIfNewPinExists(String newPin, String username);


    Mono<AccessTokenWrapper> demo(String username);

    Mono<UniversalResponse> getDemoPins(String username);

    boolean validateUserPin(String username, String pin);
}
