package io.eclectics.sessionsauthserver.api;

import io.eclectics.sessionsauthserver.other.AccessTokenWrapper;
import io.eclectics.sessionsauthserver.other.UniversalResponse;
import io.eclectics.sessionsauthserver.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/demo")
public class DemoController {

    private final UserService userService;

    @PostMapping("/user")
    public Mono<ResponseEntity<AccessTokenWrapper>> demo(@RequestParam String username) {
        return userService.demo(username).map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

    @PostMapping("/demo-pin")
    private Mono<ResponseEntity<UniversalResponse>> getDemoPins(@RequestParam String username) {
        return userService.getDemoPins(username)
                .map(response -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(response));
    }
}
