package io.eclectics.sessionsauthserver.util;

import io.eclectics.sessionsauthserver.other.UserDetailsWrapper;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.codec.multipart.Part;

import java.security.SecureRandom;
import java.util.Map;

@Slf4j
@UtilityClass
public class GeneralUtils {

    public static UserDetailsWrapper toUserDetailsWrapper(Map<String, Part> formData) {
        return UserDetailsWrapper.builder()
                .username(formData.get("username").name())
                .password(formData.get("password").toString())
                .grant_type(formData.get("grant_type").toString())
                .build();
    }

    public static String generate4DigitsOTP(){
        SecureRandom r = new SecureRandom();
        int low = 1000;
        int high = 9999;
        int result = r.nextInt(high-low) + low;
        return String.valueOf(result);

    }

    public static String generate6DigitsOtp(){
        SecureRandom r = new SecureRandom();
        int low = 100000;
        int high = 999999;
        int result = r.nextInt(high-low) + low;
        return String.valueOf(result);

    }
}
