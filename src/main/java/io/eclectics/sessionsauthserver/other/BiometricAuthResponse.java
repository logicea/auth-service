package io.eclectics.sessionsauthserver.other;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BiometricAuthResponse {
        private int version;
        private int key;
        private boolean prediction;
        private int distance;

}
