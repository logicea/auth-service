package io.eclectics.sessionsauthserver.other;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenWrapper {
    private int status;
    private String message;
    @JsonIgnore
    private String username;
    private String access_token;
    private String token_type;
    private String refresh_token;
    private Long expires_in;
    private String scope;
    private String jti;

    private boolean changePin;
    private boolean setSecQns;
    private String language;



    public AccessTokenWrapper(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public AccessTokenWrapper(int statusCode, String message, String userName) {
        this.status = statusCode;
        this.message = message;
        this.username = userName;
    }
}
