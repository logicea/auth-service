package io.eclectics.sessionsauthserver.other;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidateUserPinWrapper {
    private String username;
    private String pin;
}
