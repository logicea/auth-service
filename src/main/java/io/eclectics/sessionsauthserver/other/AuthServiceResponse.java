package io.eclectics.sessionsauthserver.other;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthServiceResponse {
    private int status;
    private String message;
    private JsonObject data;

    public AuthServiceResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public AuthServiceResponse(int status, String message, JsonObject data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
