package io.eclectics.sessionsauthserver.other;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class DemoPinWrapper {
    private Long id;
    private String demoPin;
    private String username;
    private boolean active;
    private LocalDateTime createdOn;
}
