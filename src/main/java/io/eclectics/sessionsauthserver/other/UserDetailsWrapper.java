package io.eclectics.sessionsauthserver.other;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetailsWrapper {
    private String username;
    private String password;
    private String grant_type;
}
