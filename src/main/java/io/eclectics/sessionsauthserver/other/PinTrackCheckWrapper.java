package io.eclectics.sessionsauthserver.other;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PinTrackCheckWrapper {
    private String username;
    private String newPin;
}
