package io.eclectics.sessionsauthserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "message_templates")
public class MessageTemplates extends Auditable {
    String template;
    String type;
    String language;
}
