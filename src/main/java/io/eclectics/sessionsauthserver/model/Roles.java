package io.eclectics.sessionsauthserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user_roles")
public class Roles implements Serializable {
    @Id
    private long id;

    private String name;

    private String rules;

    private String resourceId;
}

