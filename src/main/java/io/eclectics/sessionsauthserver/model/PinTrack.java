package io.eclectics.sessionsauthserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_pin_track")
public class PinTrack extends Auditable {
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column("new_pin")
    private String newPin;

    private Boolean status;
}
