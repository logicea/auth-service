package io.eclectics.sessionsauthserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class Auditable {
    @Id
    private long id;

    @CreatedBy
    protected String createdBy;

    @CreatedDate
    protected LocalDateTime createdOn;

    @LastModifiedBy
    protected String lastModifiedBy;

    @LastModifiedDate
    protected LocalDateTime lastModifiedDate;

    private boolean softDelete;

}
