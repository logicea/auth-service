package io.eclectics.sessionsauthserver.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Data
@Table(name = "oauth_client_details")
public class OauthClientDetails implements Serializable {
    @Id
    @Column("client_id")
    String clientId;
    @Column("resource_ids")
    String resourceIds;
    @Column("client_secret")
    String clientSecret;
    String scope;
    @Column("authorized_grant_types")
    String authorizedGrantTypes;
    @Column("web_server_redirect_uri")
    String webServerRedirectUri;
    String authorities;
    @Column("access_token_validity")
    long accessTokenValidity;
    @Column("refresh_token_validity")
    long refreshTokenValidity;
    @Column("additional_information")
    String additionalInformation;
    String autoapprove;
}
