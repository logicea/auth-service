package io.eclectics.sessionsauthserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users extends Auditable {
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private LocalDateTime lastLogin;

    private String deviceId;

    private String channel;

    private String phoneNumber;
    private String demoPin;

    private boolean firstTimeLogin;

    private boolean changePin = true;

    private boolean setSecQns = true;

    private boolean active = false;

    private boolean blocked = false;

    private long loginAttempts = 0;

    private String email;

    private String language;

    @Column("roles_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long roleId;

    @Transient
    private Users deactivatedBy;

    @Transient
    private Roles roles;

}
