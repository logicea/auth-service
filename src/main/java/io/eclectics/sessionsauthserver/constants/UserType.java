package io.eclectics.sessionsauthserver.constants;

public enum UserType {
    CUSTOMER, ADMIN
}
