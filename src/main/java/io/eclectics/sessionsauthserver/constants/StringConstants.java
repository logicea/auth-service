package io.eclectics.sessionsauthserver.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringConstants {

    public static final String PHONE_NUMBER_REGEX = "^(\\s*|(254)([71])\\d{8})$";
}
