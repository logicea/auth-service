package io.eclectics.sessionsauthserver.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class KafkaTopics {
    public static final String AUTH_AUDIT = "login-logout-topic";
    public static final String SMS_TOPIC = "mb-sms-topic";
    public static final String EMAIL_TOPIC = "mb-email-topic";
}
