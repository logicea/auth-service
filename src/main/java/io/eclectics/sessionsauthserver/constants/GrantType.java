package io.eclectics.sessionsauthserver.constants;

public enum GrantType {
    PASSWORD, FACE, VOICE
}

