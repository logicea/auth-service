package io.eclectics.sessionsauthserver;

import io.eclectics.sessionsauthserver.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
@EnableR2dbcRepositories
public class SessionsAuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessionsAuthServerApplication.class, args);
	}

}
