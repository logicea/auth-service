package io.eclectics.sessionsauthserver.repository;

import io.eclectics.sessionsauthserver.model.OauthClientDetails;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface OAuthClientRepository extends ReactiveCrudRepository<OauthClientDetails, String> {

    Mono<OauthClientDetails> findByClientId(String clientId);

}
