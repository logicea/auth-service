package io.eclectics.sessionsauthserver.repository;

import io.eclectics.sessionsauthserver.model.Users;
import io.eclectics.sessionsauthserver.other.DemoPinWrapper;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends ReactiveCrudRepository<Users, Long> {

    Mono<Users> findByPhoneNumberAndActive(String phoneNumber, Boolean active);

    Mono<Users> findByEmailAndActive(String email, Boolean active);

    Flux<Users> findUsersByEmailOrPhoneNumber(String email, String phoneNumber);

    Mono<Users> findByEmailAndChannelEquals(String username, String channel);

    Mono<Users> findByPhoneNumberAndChannelEquals(String username, String channel);

    Mono<Users> findByUsername(String loggedPrincipal);


    Mono<Users> findByPhoneNumber(String phone);

    Mono<DemoPinWrapper> findByUsernameAndActiveOrderByIdDesc(String username, Boolean active);


}
