package io.eclectics.sessionsauthserver.repository;

import io.eclectics.sessionsauthserver.model.MessageTemplates;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 *DAO for MessageTemplates entity.
 */
public interface MessageTemplatesRepository extends ReactiveCrudRepository<MessageTemplates, Long> {
    /**
     * Find by type and language message templates.
     *
     * @param type     the type
     * @param language the language
     * @return the message templates
     */
    Mono<MessageTemplates>  findByTypeAndLanguage(String type, String language);

    /**
     * Count by type and language int.
     *
     * @param type     the type
     * @param language the language
     * @return the int
     */
    int countByTypeAndLanguage(String type, String language);
}
