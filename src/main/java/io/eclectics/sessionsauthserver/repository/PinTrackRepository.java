package io.eclectics.sessionsauthserver.repository;

import io.eclectics.sessionsauthserver.model.PinTrack;
import io.eclectics.sessionsauthserver.model.Users;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PinTrackRepository extends ReactiveCrudRepository<PinTrack, Long> {
//    Mono<PinTrack> findByUsername(String username);

    @Query("select * from user_pin_track where username = :name ORDER BY created_on DESC Limit 10")
    Flux<PinTrack> findByUsername(String name);

}
