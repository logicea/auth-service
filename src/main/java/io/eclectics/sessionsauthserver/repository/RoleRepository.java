package io.eclectics.sessionsauthserver.repository;

import io.eclectics.sessionsauthserver.model.Roles;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;


public interface RoleRepository extends ReactiveCrudRepository<Roles, Long> {
}
