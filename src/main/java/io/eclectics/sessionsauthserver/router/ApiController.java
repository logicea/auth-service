package io.eclectics.sessionsauthserver.router;

import com.google.gson.Gson;
import io.eclectics.sessionsauthserver.annotations.MeasureTime;
import io.eclectics.sessionsauthserver.other.*;
import io.eclectics.sessionsauthserver.service.AuditService;
import io.eclectics.sessionsauthserver.service.RouterService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/oauth")
public class ApiController {
    private final Gson gson;
    private final RouterService routerService;
    private final AuditService auditService;


    @PostMapping(path = "/login", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Mono<ResponseEntity<AccessTokenWrapper>> loginWithPassword(ServerWebExchange exchange, Authentication authentication) {

        String ipAddress = auditService.extractIpAddress(exchange);
        String hostName = auditService.extractRemoteHostName(exchange);

        log.info("Address " + ipAddress + " Host Name " + hostName);

        return exchange.getFormData()
                .flatMap(map -> {
                    String username = map.getFirst("username");
                    String password = map.getFirst("pin");
                    String grantType = map.getFirst("grant_type");

                    if (username == null || username.isBlank())
                        return Mono.error(new IllegalArgumentException("Username field not found!"));
                    if (password == null || password.isBlank())
                        return Mono.error(new IllegalArgumentException("PIN field not found!"));
                    if (grantType == null || grantType.isBlank())
                        return Mono.error(new IllegalArgumentException("Grant Type field not found!"));
                    return routerService.loginUser(username.trim(), password.trim(), grantType.trim(), authentication);
                });
    }

    @MeasureTime
    @PostMapping("/refresh")
    @Timed(value = "jwt_token_refresh")
    public Mono<ResponseEntity<AccessTokenWrapper>> refreshToken(@RequestParam String token, Authentication authentication) {
        return routerService.refreshToken(token, authentication)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }




}
