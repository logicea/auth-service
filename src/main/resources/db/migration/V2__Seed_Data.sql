
-- auto-generated definition
create table user_roles
(
    id                 bigint auto_increment
        primary key,
    name               varchar(255)         null,
    resource_id        longtext             null,
    rules              longtext             null,
    created_by         varchar(255)         null,
    created_on         datetime(6)          null,
    last_modified_by   varchar(255)         null,
    last_modified_date datetime(6)          null,
    soft_delete        tinyint(1) default 0 null
);


