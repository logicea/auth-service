-- auto-generated definition
create table user_pin_track
(
    id                 bigint auto_increment
        primary key,
    username           varchar(255)         null,
    new_pin            varchar(255)         null,
    status             tinyint(1) default 0 null,
    created_by         varchar(255)         null,
    created_on         datetime(6)          null,
    last_modified_by   varchar(255)         null,
    last_modified_date datetime(6)          null,
    soft_delete        tinyint(1) default 0 null
);

